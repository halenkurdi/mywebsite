const navSlid = () => {

    //KNOWING WHEN THE BURGER IS CLICKED 
    const burger = document.querySelector(".burger");
    //the UL elements 
    const nav = document.querySelector('.nav-links');
    //the LI of the UL
    const navLinks = document.querySelectorAll('.nav-links li');



    burger.addEventListener('click', () => {
        //ADDING A CLASS TO THE UL
        nav.classList.toggle("nav-active"); //translates back the list to 0



        //adding the LI with animarion 
        navLinks.forEach((link, index) => {
            if (link.style.animation) {
                link.style.animation = '';
            } else {
                link.style.animation = `navLinksFade 0.5s ease forwards ${index/5 +0.5}s`;
            }

        });


        //burger animation the buger changes to X slow with a keyframe in the css
        burger.classList.toggle('toggle');
    });

    //animate links

}

navSlid();


const login = () => {
    const loginBtn = document.querySelector(".dropbtn");
    const dropdownContent = document.querySelector(".dropdown-content");

    loginBtn.addEventListener('click', () => {
        dropdownContent.classList.toggle('show');
        loginBtn.classList.toggle('active');
    });

}


login();